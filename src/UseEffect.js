import React, {Component, useState, useContext, useEffect} from "react";

import ReactDOM from 'react-dom'

const MyContext = React.createContext();

const App = () => {

    const [value, setValue] = useState(0);
    const [visible, setVisible] = useState(true);

  if (visible) {

      return (
          <div>
              <button onClick={() => setValue((v) => v +1)}>
                  +
              </button>
              <button onClick={() => setVisible(false)}>hide</button>
              {/*<HookCounter value={value}/>*/}
              {/*<ClassCounter value={value}/>*/}
              <Notification/>

          </div>
      );
  } else {
      return (
          <button onClick={() => setVisible(true)}>show</button>
      );
  }

};

const HookCounter = ({value}) => {

 useEffect(
     () => {
         console.log('mount');
         return () => console.log('unmount')
     }, []);
 useEffect(() => console.log('update'), [value]);

    return <p>{value}</p>;


};

const Notification = () => {

    // const not = () => {
    //
    //     return <p>Notification</p>
    //
    // };

    const [visible, setVisible] = useState(true);

    useEffect(() => {
        const timeout = setTimeout(() => setVisible(false), 3000);
        return () => clearTimeout(timeout);
    }, []);

    return (
        <div>
            {visible&&<p>Notification</p>}
        </div>
    )
};

class ClassCounter extends Component {

    componentDidMount() {
        console.log('class: mount');
    }

    componentDidUpdate(props) {
        console.log('class: update');
    }

    componentWillUnmount() {
        console.log('class: unmount')
    }

    render() {
        return <p>{this.props.value}</p>
    }


}




// const Child = () => {
//
//     const valueUse = useContext(MyContext);
//
//     return <p>{valueUse}</p>
//     // return (
//     //     <MyContext.Consumer>
//     //         {(value) => {
//     //             return (
//     //                 <p>{value}</p>
//     //             );
//     //
//     //         }}
//     //     </MyContext.Consumer>
//     //
//     // );
// };

ReactDOM.render(<App/>, document.getElementById('root'));