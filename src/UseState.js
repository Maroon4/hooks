import React, {useState} from 'react';
import ReactDOM from 'react-dom';


const App = () => {

    return (
        <div>
            <HooksUse/>
        </div>
    );

};

const HooksUse = () => {

    const [color, setColor] = useState('grey');
    const [fontSize, setFontSize] = useState(10);



    return (
        <div style={ {padding: 10, backgroundColor: color, fontSize: `${fontSize}px`}}>
            <p>Hello</p>
            <button
                onClick={() => setColor('white')}>
                White
            </button>
            <button
                onClick={() => setColor('grey')}>
                Grey
            </button>
            <button onClick={() => setFontSize((s) => s + 2)}>+</button>
            <button onClick={() => setFontSize((s) => s - 2)}>-</button>

        </div>
    )


};


ReactDOM.render(<App/>, document.getElementById('root'));


